{
  "layers": [
    {
      "name": "LayoutView #document",
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutSVGPath polygon id='polygon'",
          "rect": [264, 258, 165, 67],
          "reason": "full"
        },
        {
          "object": "LayoutSVGPath polygon id='polygon'",
          "rect": [264, 218, 164, 107],
          "reason": "full"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [265, 259, 163, 65],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [265, 219, 162, 105],
          "reason": "bounds change"
        }
      ]
    }
  ],
  "objectPaintInvalidations": [
    {
      "object": "LayoutSVGRoot svg",
      "reason": "bounds change"
    },
    {
      "object": "LayoutSVGPath polygon id='polygon'",
      "reason": "full"
    }
  ]
}

